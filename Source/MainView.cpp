/*************************************************************************************

	Chameleon - cross-platform plugin adapter - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	GPL v3 LINKING EXCEPTION
	
	You are hereby permitted to load (optional dynamic linking) any 
	independent dynamic library that implements the abstract concepts
	of the supported audio plugin formats, regardless of their licensing 
	model; so long as you satisfy and comply to all the terms of 
	aforementioned. An independent module is a module which is not derived 
	from or based on this library.

	If you modify this library, you may extend this exception to your 
	version of the library, but you are not obliged to do so. If you do 
	not wish to do so, delete this exception statement from your version.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:MainView.cpp
	
		Implementation of MainView.h

*************************************************************************************/

#include "ChameleonEngine.h"
#include "MainView.h"
#include <cpl/Resources.h>
#include "CLoader.h"

const ProgramInfo cpl::programInfo
{
	"Chameleon",
	cpl::Version::fromParts(0, 1, 0),
	"Lightbridge",
	"cmln",
	false,
	nullptr,
	""
};

namespace Chameleon
{
	MainView::MainView(Engine * ownerFilter)
	: AudioProcessorEditor (ownerFilter), bck(cpl::CResourceManager::instance().getImage("background.png")),
	pluginLoaded(false), pluginList(), pluginManager(), bscan("Scan..."), engine(ownerFilter), selectedElement(nullptr)
	{
		pluginManager.addDefaultFormats();
		statusLabel.setText("No plugin opened");
		statusLabel.setColour(juce::Colours::blue);
		statusLabel.setJustification(juce::Justification::centredRight);
		statusLabel.setFontSize(cpl::TextSize::normalText);
		addAndMakeVisible(statusLabel);
		buttons.push_back(&blib);
		buttons.push_back(&bscan);
		buttons.push_back(&bload);
		buttons.push_back(&bsave);
		blib.setButtonText("Open library");
		blib.setClickingTogglesState(true);
		blib.setVisible(true);
		bload.setButtonText("Disguise!");
		bsave.setButtonText("Save as new plugin");
		
		juce::File xmlPlugins(cpl::Misc::DirectoryPath() + "/resources/plugins.xml");
		if(xmlPlugins.existsAsFile())
		{
			juce::ScopedPointer<juce::XmlElement> e(juce::XmlDocument::parse(xmlPlugins));
			pluginList.recreateFromXml(*e);
		}
		
		pluginView = new juce::PluginListComponent(pluginManager, pluginList, juce::File::nonexistent, nullptr);
		pluginView->setSelectionListener(this);
		
		addChildComponent(pluginView);
		
		for(auto & button : buttons)
		{
			button->setColour(juce::TextButton::ColourIds::textColourOffId, cpl::CColours::lightgoldenrodyellow);
			button->setColour(juce::TextButton::ColourIds::buttonColourId, cpl::CColours::darkgrey);
			button->addListener(this);
			addChildComponent(button);
		}
		
		
		// This is where our plugin's editor size is set.
		setSize (bck.getWidth() ? bck.getWidth() : 400, bck.getHeight() ? bck.getHeight() : 300);
		
	}
	void MainView::changeListenerCallback (juce::ChangeBroadcaster *source)
	{
		cpl::Misc::MsgBox("Hel");
		
	}

	void MainView::onPluginSelection(int index)
	{
		auto plugin = pluginList.getType(index);
		std::string temp;
		statusLabel.setText(std::string("Selected plugin: ") + plugin->name.toStdString());
		auto ret = Chameleon::Engine::getLoader().isPluginCompatible(*plugin, temp);
		statusLabel.setColour(ret ? juce::Colours::darkgreen : juce::Colours::red);
		if(!ret)
		{
			statusLabel.setText(temp);
			pluginSelected = false;
			selectedElement = nullptr;
		}
		else
		{
			pluginSelected = true;
			selectedElement = plugin;
		}
		blib.triggerClick();
	}

	void MainView::resized()
	{
		if(!pluginLoaded)
		{
			int count = 0;
			blib.setBounds(5, 5, 100, 20);
			bload.setBounds(++count * blib.getWidth() + 5, 5, 100, 20);
			bsave.setBounds(++count * blib.getWidth() + 5, 5, 100, 20);
			statusLabel.setBounds(5, getHeight() - 20, getWidth() - 10, 20);
			pluginView->setBounds(0, blib.getBounds().getBottom() + 5, getWidth(), getHeight() - (blib.getBounds().getBottom()) - 7);
			bscan.setBounds(getWidth() - 105, getHeight() - blib.getHeight() - 5, 100, 20);
		}
	}

	void MainView::buttonClicked(juce::Button * b)
	{
		static juce::VSTPluginFormat lolol;
		if(b == &blib)
		{
			if(blib.getToggleState())
			{
				blib.setButtonText("Close library...");
				pluginView->setVisible(true);
				bscan.setVisible(true);
				statusLabel.setVisible(false);
				bload.setVisible(false);
				bsave.setVisible(false);
			}
			else
			{
				blib.setButtonText("Open library...");
				pluginView->setVisible(false);
				statusLabel.setVisible(true);
				bscan.setVisible(false);
				if(pluginSelected)
				{
					bload.setVisible(true);
					bsave.setVisible(true);
					
				}
			}
		}
		else if(b == &bscan)
		{
			juce::PopupMenu p;
			for(int i = 0; i < pluginManager.getNumFormats(); ++i)
			{
				p.addItem(i + 1, pluginManager.getFormat(i)->getName());
			}
			auto ret = p.show();
			if(ret && ret < pluginManager.getNumFormats() + 1) // user clicked a valid index
			{
				pluginView->scanFor(*pluginManager.getFormat(ret - 1));
				
			}
		}
		else if(b == &bload)
		{
			if(pluginSelected && selectedElement)
			{
				auto ret = engine->getLoader().disguiseAs(engine, *selectedElement);
				std::string text;
				if(ret)
				{
					text = selectedElement->name.toStdString() + " loaded succesfully! Reopen the editor.";
				}
				else
				{
					text = "Error loading " + selectedElement->name.toStdString() + " for some reason...";
				}
				cpl::Misc::MsgBox(text);
			}
			
		}
		else if(b == &bsave)
		{
			if(pluginSelected && selectedElement)
			{
				juce::PopupMenu p;
				for(int i = 0; i < pluginManager.getNumFormats(); ++i)
				{
					p.addItem(i + 1, pluginManager.getFormat(i)->getName());
				}
				auto ret = p.show();
				if(ret && ret < pluginManager.getNumFormats() + 1)
				{
					
					CLoader::PluginFormat pf = CLoader::PluginFormat::VST;
					auto format = pluginManager.getFormat(ret - 1)->getName();
					if(format.contains("VST3"))
					{
						pf = CLoader::PluginFormat::VST3;
					}
					else if(format.contains("VST"))
					{
						
						pf = CLoader::PluginFormat::VST;
					}
					else if(format.contains("AudioUnit"))
					{
						pf = CLoader::PluginFormat::AudioUnit;
					}
					juce::File initialDir = juce::File::getSpecialLocation(juce::File::SpecialLocationType::userHomeDirectory).getFullPathName() + "/library/audio/plug-ins";
					juce::FileChooser fc("Select a location for your new " + format, initialDir);
					if(fc.browseForFileToSave(true))
					{
						
						auto ret = engine->getLoader().saveAsNewPlugin(*selectedElement, fc.getResult().getFullPathName().toStdString(), pf);
						if(ret)
						{
							statusLabel.setColour(juce::Colours::darkgreen);
							statusLabel.setText("New plugin saved succesfully!");
						}
						else
						{
							statusLabel.setColour(juce::Colours::red);
							statusLabel.setText("An error occured while saving...");
						}
					}
				}
			}
		}
	}

	MainView::~MainView()
	{
		auto ret = pluginList.createXml();
		ret->writeToFile(juce::File(cpl::Misc::DirectoryPath() + "/resources/plugins.xml"), juce::String::empty);
		delete ret;
	}

	//==============================================================================
	void MainView::paint (juce::Graphics& g)
	{
		g.fillAll (juce::Colours::white);
		g.setColour (juce::Colours::black);
		g.drawImageAt(bck, 0, 0);
	}
};