/*************************************************************************************

	Chameleon - cross-platform plugin adapter - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	GPL v3 LINKING EXCEPTION
	
	You are hereby permitted to load (optional dynamic linking) any 
	independent dynamic library that implements the abstract concepts
	of the supported audio plugin formats, regardless of their licensing 
	model; so long as you satisfy and comply to all the terms of 
	aforementioned. An independent module is a module which is not derived 
	from or based on this library.

	If you modify this library, you may extend this exception to your 
	version of the library, but you are not obliged to do so. If you do 
	not wish to do so, delete this exception statement from your version.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:ChameleonEngine.cpp
	
		Implementation of ChameleonEngine.h

*************************************************************************************/


#include "ChameleonEngine.h"
#include "MainView.h"
#include <sstream>
#include "CLoader.h"

juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
	
	if(juce::AudioPluginInstance * instance = Chameleon::Engine::getLoader().loadDefaultPlugin())
	{
		return instance;
	}
	
	return new Chameleon::Engine();
}

namespace Chameleon
{

	struct StateData
	{
		std::size_t xmlSize, pluginSize, totalSize;
		
		const void * getXmlDataConst() const { return reinterpret_cast<const char *>(this) + sizeof(*this); }
		const void * getPluginDataConst() const { return reinterpret_cast<const char *>(this) + sizeof(*this) + xmlSize; }
		void * getXmlData() { return reinterpret_cast<char *>(this) + sizeof(*this); }
		void * getPluginData() { return reinterpret_cast<char *>(this) + sizeof(*this) + xmlSize; }
	};

	CLoader & Engine::getLoader()
	{
		static CLoader loader;
		return loader;
	}

	//==============================================================================
	void Engine::getStateInformation (juce::MemoryBlock& destData)
	{
		if(isLoaded)
		{
			juce::MemoryBlock xmlInfo;
			juce::XmlElement * xml(desc.createXml());
			if(xml)
			{
				copyXmlToBinary(*xml, xmlInfo);
				juce::MemoryBlock pluginInfo;
				instance->getStateInformation(pluginInfo);
				
				auto totalSize = sizeof(StateData) + xmlInfo.getSize() + pluginInfo.getSize();
				StateData * data = reinterpret_cast<StateData*>(std::malloc(totalSize));
				if(data)
				{
					data->totalSize = totalSize;
					data->pluginSize = pluginInfo.getSize();
					data->xmlSize = xmlInfo.getSize();
					std::memcpy(data->getXmlData(), xmlInfo.getData(), xmlInfo.getSize());
					std::memcpy(data->getPluginData(), pluginInfo.getData(), pluginInfo.getSize());
					destData.append(data, data->totalSize);
					std::free(data);
				}
				
				delete xml;
			}
		}
		// You should use this method to store your parameters in the memory block.
		// You could do that either as raw data, or use the XML or ValueTree classes
		// as intermediaries to make it easy to save and load complex data.
	}

	void Engine::setStateInformation (const void* data, int sizeInBytes)
	{
		if(sizeInBytes)
		{
			const StateData * newData = reinterpret_cast<const StateData *>(data);
			if(newData && newData->totalSize >= sizeof(StateData) && newData->totalSize == sizeInBytes)
			{
				juce::XmlElement * xml = getXmlFromBinary(newData->getXmlDataConst(), newData->xmlSize);
				if(xml)
				{
					if(desc.loadFromXml(*xml))
					{
						getLoader().disguiseAs(this, desc);
						if(isLoaded)
						{
							instance->setStateInformation(newData->getPluginDataConst(), newData->pluginSize);
						}
						
					}
					delete xml;
				}
			}
			
		}
		// You should use this method to restore your parameters from this memory block,
		// whose contents will have been created by the getStateInformation() call.
	}

	void Engine::setInstance(juce::AudioPluginInstance * i, const juce::PluginDescription & desc)
	{
		instance = i;
		isLoaded = !!i;
		if(isLoaded)
		{
			auto & lock = getCallbackLock();
			lock.enter();
			this->desc = desc;
			instance->prepareToPlay(getSampleRate(), this->getBlockSize());
			updateHostDisplay();
			lock.exit();
		}
		
		
	}


	Engine::Engine()
	: isLoaded(false)
	{

	}

	Engine::~Engine()
	{
		
	}

	//==============================================================================
	const juce::String Engine::getName() const
	{
		if(isLoaded)
		{
			return "Engine::" + instance->getName();
		}
		return JucePlugin_Name;
	}

	int Engine::getNumParameters()
	{
		if(isLoaded)
		{
			return instance->getNumParameters();
		}
		return 0;
	}

	float Engine::getParameter (int index)
	{
		if(isLoaded)
		{		
			return instance->getParameter(index);
		}
		return 0.0f;
	}

	void Engine::setParameter (int index, float newValue)
	{
		if(isLoaded)
		{
			return instance->setParameter(index, newValue);
		}
	}

	const juce::String Engine::getParameterName (int index)
	{
		if(isLoaded)
		{
			return instance->getParameterName(index);
		}
		return juce::String::empty;
	}

	const juce::String Engine::getParameterText (int index)
	{
		if(isLoaded)
		{
			return instance->getParameterText(index);
		}
		return juce::String::empty;
	}

	const juce::String Engine::getInputChannelName (int channelIndex) const
	{
		if(isLoaded)
		{
			return instance->getInputChannelName(channelIndex);
		}
		return juce::String (channelIndex + 1);
	}

	const juce::String Engine::getOutputChannelName (int channelIndex) const
	{
		if(isLoaded)
		{
			return instance->getOutputChannelName(channelIndex);
		}
		return juce::String (channelIndex + 1);
	}

	bool Engine::isInputChannelStereoPair (int index) const
	{
		if(isLoaded)
		{
			return instance->isInputChannelStereoPair(index);
		}
		return true;
	}

	bool Engine::isOutputChannelStereoPair (int index) const
	{
		if(isLoaded)
		{
			return instance->isOutputChannelStereoPair(index);
		}
		return true;
	}

	bool Engine::acceptsMidi() const
	{
		if(isLoaded)
		{
			return instance->acceptsMidi();
		}
		return JucePlugin_WantsMidiInput;
	}

	bool Engine::producesMidi() const
	{
		if(isLoaded)
		{
			return instance->producesMidi();
		}
		return JucePlugin_ProducesMidiOutput;
	}

	bool Engine::silenceInProducesSilenceOut() const
	{
		if(isLoaded)
		{
			return instance->silenceInProducesSilenceOut();
		}
		return JucePlugin_SilenceInProducesSilenceOut;
	}

	double Engine::getTailLengthSeconds() const
	{
		if(isLoaded)
		{
			return instance->getTailLengthSeconds();
		}
		return 0.0;
	}

	int Engine::getNumPrograms()
	{
		if(isLoaded)
		{
			return instance->getNumPrograms();
		}
		return 0;
	}

	int Engine::getCurrentProgram()
	{
		if(isLoaded)
		{
			return instance->getCurrentProgram();
		}
		return 0;
	}

	void Engine::setCurrentProgram (int index)
	{
		if(isLoaded)
		{
			return instance->setCurrentProgram(index);
		}
	}

	const juce::String Engine::getProgramName (int index)
	{
		if(isLoaded)
		{
			return instance->getProgramName(index);
		}
		return juce::String::empty;
	}

	void Engine::changeProgramName (int index, const juce::String& newName)
	{
		if(isLoaded)
		{
			return instance->changeProgramName(index, newName);
		}
	}

	//==============================================================================
	void Engine::prepareToPlay (double sampleRate, int samplesPerBlock)
	{
		if(isLoaded)
		{
			return instance->prepareToPlay(sampleRate, samplesPerBlock);
		}
		// Use this method as the place to do any pre-playback
		// initialisation that you need..
	}

	void Engine::releaseResources()
	{
		if(isLoaded)
		{
			return instance->releaseResources();
		}
		// When playback stops, you can use this as an opportunity to free up any
		// spare memory, etc.
	}

	void Engine::processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer& midiMessages)
	{
		if(isLoaded)
		{
			return instance->processBlock(buffer, midiMessages);
		}
		else
		{
			// This is the place where you'd normally do the guts of your plugin's
			// audio processing...
			for (int channel = 0; channel < getNumInputChannels(); ++channel)
			{
				float* channelData = buffer.getSampleData (channel);

				// ..do something to the data...
			}

			// In case we have more outputs than inputs, we'll clear any output
			// channels that didn't contain input data, (because these aren't
			// guaranteed to be empty - they may contain garbage).
			for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
			{
				buffer.clear (i, 0, buffer.getNumSamples());
			}
		}
	}

	//==============================================================================
	bool Engine::hasEditor() const
	{
		if(isLoaded)
		{
			return instance->hasEditor();
		}
		return true;
	}

	juce::AudioProcessorEditor* Engine::createEditor()
	{
		if(isLoaded)
		{
			return instance->createEditor();
		}
		else
		{
			return new ChameleonAudioProcessorEditor(this);
		}
		return nullptr;
	}


};
