/*************************************************************************************

	Chameleon - cross-platform plugin adapter - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	GPL v3 LINKING EXCEPTION
	
	You are hereby permitted to load (optional dynamic linking) any 
	independent dynamic library that implements the abstract concepts
	of the supported audio plugin formats, regardless of their licensing 
	model; so long as you satisfy and comply to all the terms of 
	aforementioned. An independent module is a module which is not derived 
	from or based on this library.

	If you modify this library, you may extend this exception to your 
	version of the library, but you are not obliged to do so. If you do 
	not wish to do so, delete this exception statement from your version.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:CLoader.h
	
		The interface for loading and disguising plugins through an Chameleon::Engine.

*************************************************************************************/

#ifndef CHAMELEON_CLOADER_H
	#define CHAMELEON_CLOADER_H

	#include <cpl/Common.h>

	#include <cpl/Utility.h>
	#include <cpl/CMutex.h>
	#include <typeinfo>


	namespace Chameleon
	{
		class Engine;

		class CLoader : public cpl::CMutex::Lockable, public cpl::Utility::CNoncopyable
		{

		public:
			
			enum class PluginFormat
			{
				VST, VST3, AudioUnit, RTAS, AAX
				
			};
			CLoader();
			juce::AudioPluginInstance * loadPlugin(Engine * engine, const std::string & pluginPath);
			juce::AudioPluginInstance * loadPlugin(Engine * engine, const juce::PluginDescription & desc);
			bool disguiseAs(Engine * engine, const juce::PluginDescription & desc);
			bool isPluginCompatible(const juce::PluginDescription & desc, std::string & error);
			juce::PluginDescription getDescription(const std::string & pluginPath);
			juce::AudioPluginInstance * loadDefaultPlugin();
			bool saveAsNewPlugin(const juce::PluginDescription & desc, const std::string & path, PluginFormat format);
			const char * platformExtensionFor(PluginFormat format);
		private:

			bool editResources(juce::File resource, const juce::PluginDescription & desc, juce::File dest = juce::File::nonexistent);
			bool editInfoPlist(juce::File plist, const juce::PluginDescription & desc, juce::File dest = juce::File::nonexistent);
			std::string getPListDTD();
			juce::AudioPluginFormatManager pluginManager;
			juce::KnownPluginList pluginList;
			
			juce::PluginDescription desc;
			juce::VSTPluginFormat format;
			juce::OwnedArray<juce::PluginDescription> pluginArray;
		};
	};
#endif