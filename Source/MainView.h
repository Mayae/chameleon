/*************************************************************************************

	Chameleon - cross-platform plugin adapter - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	GPL v3 LINKING EXCEPTION
	
	You are hereby permitted to load (optional dynamic linking) any 
	independent dynamic library that implements the abstract concepts
	of the supported audio plugin formats, regardless of their licensing 
	model; so long as you satisfy and comply to all the terms of 
	aforementioned. An independent module is a module which is not derived 
	from or based on this library.

	If you modify this library, you may extend this exception to your 
	version of the library, but you are not obliged to do so. If you do 
	not wish to do so, delete this exception statement from your version.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:MainView.h
	
		The main editor.

*************************************************************************************/

#ifndef CHAMELEON_MAINVIEW_H
	#define CHAMELEON_MAINVIEW_H

	#include <cpl/common.h>
	#include "MainView.h"
	#include <cpl/gui/GraphicComponents.h>
	#include <vector>

	namespace Chameleon
	{
		class MainView
			: public juce::AudioProcessorEditor
			, juce::TextButton::Listener
			, juce::PluginListComponent::PluginComponentListener
		{
		public:
			
			MainView(Engine * ownerFilter);
			~MainView();
			void resized() override;
			void paint (juce::Graphics& g);
			
			
		private:
			virtual void onPluginSelection(int index) override;
			
			Engine * engine;
			juce::Image bck;
			juce::TextButton blib, bload, bsave, bscan;
			bool pluginSelected, pluginLoaded;
			cpl::CTextLabel statusLabel;
			virtual void buttonClicked(juce::Button * b) override;
			virtual void changeListenerCallback (juce::ChangeBroadcaster *source);
			std::vector<juce::TextButton *> buttons;
		
			juce::AudioPluginFormatManager pluginManager;
			juce::KnownPluginList pluginList;
			juce::PluginDescription * selectedElement;
			juce::ScopedPointer<juce::PluginListComponent> pluginView;
		};

	};
typedef Chameleon::MainView ChameleonAudioProcessorEditor;
#endif
