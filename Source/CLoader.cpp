/*************************************************************************************

	Chameleon - cross-platform plugin adapter - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	GPL v3 LINKING EXCEPTION
	
	You are hereby permitted to load (optional dynamic linking) any 
	independent dynamic library that implements the abstract concepts
	of the supported audio plugin formats, regardless of their licensing 
	model; so long as you satisfy and comply to all the terms of 
	aforementioned. An independent module is a module which is not derived 
	from or based on this library.

	If you modify this library, you may extend this exception to your 
	version of the library, but you are not obliged to do so. If you do 
	not wish to do so, delete this exception statement from your version.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:CLoader.cpp
 
		Implementation of CLoader.h

*************************************************************************************/

#include "CLoader.h"
#include "ChameleonEngine.h"
#include <cstdint>
#include <cctype>
#ifdef CPL_MAC
#include <cpl/CPlistEditor.h>
#include <cpl/CRsrcEditor.h>
#include <AudioUnit/AudioUnit.h>
#endif
namespace Chameleon
{

	std::string CLoader::getPListDTD()
	{
		return "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
	}

	std::string StringFromCharInt(std::int32_t input)
	{
		union IntBits
		{
			std::int32_t i;
			char b[4];
			
		};
		
		IntBits r;
		r.i = input;
		std::string ret;
		ret.resize(4);
		for(int x = 0; x < 4; x++)
		{
			ret[3 - x] = r.b[x];
		}
		return ret;
	}

	std::string AUStringIDFromString(const std::string & input)
	{
		
		union IntBits
		{
			std::int32_t i;
			char b[4];
			
		};
		std::string ret;
		IntBits hash;
		
		// hash the input string to create an integer which, using the struct above, contains 4 ascii codes.
		hash.i = juce::String(input).hashCode();
		bool hasUpperCase = false;
		// ensure one letter is capitalized and that all ascii codes are alphabetical
		// important: output must be deterministic for the same hash
		for(int x = 0; x < 4; x++)
		{
			unsigned char c = hash.b[x];
			if(!std::isalpha(c))
			{
				/*
				 this part here maps non-alphabetical chars back into alphabetical range
				 */
				if(c < 'A')
				{
					c = (c % ('Z' - 'A')) + 'A';
					
				}
				else if(c > 'Z')
				{
					c = (c % ('z' - 'a')) + 'a';
					
				}
			}
			
			if(!hasUpperCase && std::islower(c))
			{
				c = static_cast<unsigned char>(std::toupper(c));
				hasUpperCase = true;
			}
			else
			{
				hasUpperCase = true;
			}
			ret.push_back(c);
		}
		return ret;
	}
	// BIG ENDIAN!
	std::uint32_t AUIDFromString(const std::string & input)
	{
		
		union IntBits
		{
			std::uint32_t i;
			unsigned char b[4];
			
		};
		IntBits hash;
		
		// hash the input string to create an integer which, using the struct above, contains 4 ascii codes.
		hash.i = juce::String(input).hashCode();
		bool hasUpperCase = false;
		// ensure one letter is capitalized and that all ascii codes are alphabetical
		// important: output must be deterministic for the same hash
		for(int x = 0; x < 4; x++)
		{
			unsigned char & c = hash.b[x];
			if(!std::isalpha(c))
			{
				/*
				 this part here maps non-alphabetical chars back into alphabetical range
				 */
				if(c < 'A')
				{
					c = (c % ('Z' - 'A')) + 'A';
					
				}
				else if(c > 'Z')
				{
					c = (c % ('z' - 'a')) + 'a';
					
				}
			}
			
			if(!hasUpperCase && std::islower(c))
			{
				c = std::toupper(c);
				hasUpperCase = true;
			}
			else
			{
				hasUpperCase = true;
			}
		}
		return hash.i;
	}

	bool CLoader::editInfoPlist(juce::File plist, const juce::PluginDescription & desc, juce::File dest)
	{
		#ifdef CPL_MAC
			cpl::CPListEditor editor(plist);
			if(!editor.parse())
				return false;
			if(!editor.editKey("CFBundleIdentifier", ("com." + desc.manufacturerName + "." + desc.name).removeCharacters(" \n\t\r").toStdString()))
				return false;
			if(!editor.editKey("CFBundleName", (desc.name).toStdString()))
				return false;
			if(!editor.editKey("CFBundleExecutable", JucePlugin_Name))
				return false;
		
			/*
				edit the new format au specifics - needed to abstract our wrapper
			*/

			auto audioDict = editor.getKey("dict").getKey("AudioComponents").getValue().getKey("dict");
			if(audioDict.exists())
			{
				audioDict.getKey("description").setValue(desc.descriptiveName.toStdString());
				audioDict.getKey("manufacturer").setValue(AUStringIDFromString(desc.manufacturerName.toStdString()));
				audioDict.getKey("subtype").setValue(AUStringIDFromString(desc.name.toStdString()));
				audioDict.getKey("type").setValue(StringFromCharInt(JucePlugin_AUMainType));
				audioDict.getKey("name").setValue((desc.manufacturerName + ": " + "Chameleon<" + desc.name + ">").toStdString());
				audioDict.getKey("version").setValue(std::to_string(JucePlugin_VersionCode)); //desc.version.toStdString()
			
			}
			if(!editor.saveAs(dest != juce::File::nonexistent ? dest : plist))
				return false;
		
			/*
				edit old style au specifics - resource edititng
			*/
		

		#endif
		return true;
	}

	bool CLoader::editResources(juce::File resourceFile, const juce::PluginDescription & desc, juce::File dest)
	{
		#ifdef CPL_MAC
			auto subtype = AUIDFromString(desc.name.toStdString());
			auto manu = AUIDFromString(desc.manufacturerName.toStdString());
			auto mainType = JucePlugin_AUMainType;
		
		
			cpl::CRsrcEditor editor;
			std::string error;
		
			if(!editor.load(resourceFile, error))
				return false;
			editor.setAUDetails(mainType, juce::ByteOrder::swapIfLittleEndian(subtype), juce::ByteOrder::swapIfLittleEndian(manu));
			editor.setStringDetails(("Chameleon<" + desc.name + ">").toStdString(), desc.manufacturerName.toStdString(), desc.descriptiveName.toStdString());
		
			if(!editor.saveAs(error, dest))
				return false;

		#endif
		return true;
	}

	const char * CLoader::platformExtensionFor(PluginFormat format)
	{
#ifdef CPL_MAC
		switch (format)
		{
		case Chameleon::CLoader::PluginFormat::VST: return "vst";
		case Chameleon::CLoader::PluginFormat::VST3: return "vst3";
		case Chameleon::CLoader::PluginFormat::AudioUnit: 
		case Chameleon::CLoader::PluginFormat::RTAS:
		case Chameleon::CLoader::PluginFormat::AAX:
			return "component";
		}
#else
		switch (format)
		{
		case Chameleon::CLoader::PluginFormat::VST: return "dll";
		case Chameleon::CLoader::PluginFormat::VST3: return "vst3";
		}
#endif
		return "unknown";
	}

	bool CLoader::saveAsNewPlugin(const juce::PluginDescription & desc, const std::string & path,  PluginFormat format)
	{
		std::string newpath;
		// create the correct extension

		if(path.find(platformExtensionFor(format)) != std::string::npos)
			newpath = path;
		else
			newpath = path + "." + platformExtensionFor(format);

		// something already exists
		if(juce::File(newpath).exists())
			return false;

		std::string root = newpath;
		#ifdef CPL_MAC
			std::string root += "/contents/";
		#endif

		std::string thisPath = cpl::Misc::DirectoryPath();
		juce::File parent(juce::File(thisPath).getParentDirectory().getParentDirectory());
		
		if(!parent.copyFileTo(juce::File(newpath)))
			return false;
		
		if(juce::ScopedPointer<juce::XmlElement> xml = desc.createXml())
		{
			if(!xml->writeToFile(juce::File(root + "/resources/stdplugin.xml"), juce::String::empty))
				return false;
		}
		else
			return false;
		
		if(!editInfoPlist((juce::File)(root + "/resources/properties.plist"), desc, (juce::File)(root + "Info.plist")))
			return false;
		if(!editResources((juce::File)(root + "/resources/" + JucePlugin_Name + ".rsrc"), desc))
			return false;
		return true;
	}


	juce::AudioPluginInstance * CLoader::loadPlugin(Engine * engine, const std::string & pluginPath)
	{
		return nullptr;
	}

	CLoader::CLoader()
	{
		pluginManager.addDefaultFormats();
	}

	bool CLoader::disguiseAs(Engine * engine, const juce::PluginDescription & desc)
	{
		if(juce::AudioPluginInstance * i = loadPlugin(engine, desc))
		{
			engine->setInstance(i, desc);
			return engine->isDisguised();
		}
		return false;
	}
	juce::AudioPluginInstance * CLoader::loadDefaultPlugin()
	{
		juce::File dbgbrk(cpl::Misc::DirectoryPath() + "/dbg.xml");
		if(dbgbrk.existsAsFile())
			cpl::Misc::MsgBox("Debug break");
		
		juce::File plug(cpl::Misc::DirectoryPath() + "/stdplugin.xml");
		if(plug.existsAsFile())
		{
			if(juce::ScopedPointer<juce::XmlElement> e = juce::XmlDocument::parse(plug))
			{
				desc.loadFromXml(*e);
				juce::String error;
				return pluginManager.createPluginInstance(desc, 44100, 512, error);
			}
			
		}

		return nullptr;
	}

	bool CLoader::isPluginCompatible(const juce::PluginDescription & desc, std::string & error)
	{
		if(desc.numInputChannels > JucePlugin_MaxNumInputChannels)
		{
			error = "Too many input channels";
			return false;
		}
		if(desc.numOutputChannels > JucePlugin_MaxNumOutputChannels)
		{
			error = "Too many output channels";
			return false;
		}
		if((!!desc.isInstrument) != (!!JucePlugin_IsSynth))
		{
			error = "Synth/effect mismatch";
			return false;
		}
		for(int i = 0; i < pluginManager.getNumFormats(); ++i)
		{
			if(desc.pluginFormatName.compare(pluginManager.getFormat(i)->getName()))
				return true;
		}
		error = std::string("Unsupported format: ") + desc.pluginFormatName.toStdString();
		return false;
	}

	juce::AudioPluginInstance * CLoader::loadPlugin(Engine * engine, const juce::PluginDescription & desc)
	{
		juce::String error;
		return pluginManager.createPluginInstance(desc, engine->getSampleRate(), engine->getBlockSize(), error);
		
	}

	juce::PluginDescription CLoader::getDescription(const std::string & pluginPath)
	{
		
		return juce::PluginDescription();
	}
};