/*************************************************************************************

	Chameleon - cross-platform plugin adapter - v. 0.1.0.

	Copyright (C) 2016 Janus Lynggaard Thorborg (www.jthorborg.com)

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
	GPL v3 LINKING EXCEPTION
	
	You are hereby permitted to load (optional dynamic linking) any 
	independent dynamic library that implements the abstract concepts
	of the supported audio plugin formats, regardless of their licensing 
	model; so long as you satisfy and comply to all the terms of 
	aforementioned. An independent module is a module which is not derived 
	from or based on this library.

	If you modify this library, you may extend this exception to your 
	version of the library, but you are not obliged to do so. If you do 
	not wish to do so, delete this exception statement from your version.

	See \licenses\ for additional details on licenses associated with this program.

**************************************************************************************

	file:ChameleonEngine.h
	
		The main processor and dispatcher for plugin interfaces.

*************************************************************************************/

#ifndef CHAMELEON_CHAMELEONENGINE_H
#define CHAMELEON_CHAMELEONENGINE_H

#include <cpl/common.h>
#include <cpl/Misc.h>
namespace Chameleon
{

	class CLoader;
	
	class Engine : public juce::AudioProcessor, juce::AudioProcessorListener
	{
		
	public:


		Engine();
		~Engine();
		
		bool isDisguised() { return isLoaded;}
		void setInstance(juce::AudioPluginInstance * i, const juce::PluginDescription & desc);

		void report(const std::string & text)
		{
			cpl::Misc::MsgBox(text);
		}

		static CLoader & getLoader();
		
		//==============================================================================
		void prepareToPlay (double sampleRate, int samplesPerBlock);
		void releaseResources();
		void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer& midiMessages);

		//==============================================================================
		juce::AudioProcessorEditor* createEditor();
		bool hasEditor() const;

		//==============================================================================
		const juce::String getName() const;

		int getNumParameters();

		float getParameter (int index);
		void setParameter (int index, float newValue);

		const juce::String getParameterName (int index);
		const juce::String getParameterText (int index);

		const juce::String getInputChannelName (int channelIndex) const;
		const juce::String getOutputChannelName (int channelIndex) const;
		bool isInputChannelStereoPair (int index) const;
		bool isOutputChannelStereoPair (int index) const;

		bool acceptsMidi() const;
		bool producesMidi() const;
		bool silenceInProducesSilenceOut() const;
		double getTailLengthSeconds() const;

		//==============================================================================
		int getNumPrograms();
		int getCurrentProgram();
		void setCurrentProgram (int index);
		const juce::String getProgramName (int index);
		void changeProgramName (int index, const juce::String& newName);

		//==============================================================================
		void getStateInformation (juce::MemoryBlock& destData);
		void setStateInformation (const void* data, int sizeInBytes);

		
		
	protected:
		
		virtual void audioProcessorParameterChanged (AudioProcessor* processor, int parameterIndex, float newValue) override
		{
			updateHostDisplay();
		}
	
		virtual void audioProcessorParameterChangeGestureBegin (AudioProcessor *processor, int parameterIndex) override
		{
			beginParameterChangeGesture(parameterIndex);
		}

		virtual void audioProcessorParameterChangeGestureEnd (AudioProcessor *processor, int parameterIndex) override
		{
			endParameterChangeGesture(parameterIndex);
		}

		virtual void audioProcessorChanged (AudioProcessor* processor)
		{
		}

	private:

		juce::ScopedPointer<juce::AudioPluginInstance> instance;
		bool isLoaded;
		juce::PluginDescription desc;
		
		//==============================================================================
		JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Engine)
	};
};
#endif
